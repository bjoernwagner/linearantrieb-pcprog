﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;


namespace LinearAntrieb_PCProg
{
    public partial class Form1 : Form
    {
        //Kommandos an ECU
        const byte CMD_REQUEST_STATUS   = 1;  // Status abfragen
        const byte CMD_AUTO_START       = 2;  // Automatik Start
        //const byte AUTO_PAUSE       = 3;  // Automatik Pause
        const byte CMD_AUTO_STOP        = 4;  // Alles was auch immer läuft anhalten.
        const byte CMD_MANU_NEW_POS     = 5;  // Manuell neue Position anfahren
        const byte CMD_MANU_NULL_POS    = 6;  // Position "nullen"
        const byte CMD_UNIT_RESET       = 7;  // Fehler zurücksetzen

        const byte STAT_OFFLINE     = 0;  // Keine Kommunikation
        const byte STAT_STANDBY     = 1;  // Nichts zu tun
        const byte STAT_AUTO_RUN    = 2;  // Automatik läuft
        //const byte STAT_AUTO_PAUSE  = 3;  // Automatik pausiert
        const byte STAT_MANU_RUN    = 5;  // Manuelle Bewegung
        const byte STAT_NULLEN      = 6;  // Beim Nullen
        const byte STAT_ERROR       = 11;  // Manuelle Bewegung

        IBEXCom CIBEXCom     = new IBEXCom();

        t_Paket USB_RxPaket = new t_Paket(6); //Nutzdaten
        t_Paket USB_TxPaket = new t_Paket(26); //Nutzdaten
     

        public Form1()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false; //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // Hole alle verfügbaren Com Schnittstellen
            COMBO_COM_Schnittstelle.Items.Clear();
            COMBO_COM_Schnittstelle.Items.AddRange(SerialPort.GetPortNames());
            if (COMBO_COM_Schnittstelle.Items.Count != 0) COMBO_COM_Schnittstelle.SelectedIndex = 0;
            USB_TxPaket.Unit = 0x01; //Unit = Steps, Steps/s, Steps/s²
        }


        private void BUT_COM_Verbinden_Click(object sender, EventArgs e)
        {
            try
                {
                SerPort1.PortName = Convert.ToString(COMBO_COM_Schnittstelle.SelectedItem.ToString());
                SerPort1.Open();
                }
            catch (Exception ex)
                {
                MessageBox.Show("Error: " + ex.ToString(), "ERROR"); //MessageBox.Show("Fehler beim öffnen des Serialports.", "Fehler!");
                }
            
            if (SerPort1.IsOpen)
                {
                LABEL_COM_Status.Text = "Verbunden";
                LABEL_COM_Status.BackColor = Color.FromArgb(128, 255, 128);
                BUT_COM_Trennen.Enabled = true;
                COMBO_COM_Schnittstelle.Enabled = false;
                BUT_COM_Verbinden.Enabled = false;
                Timer1.Enabled = true;
                }
            else
                {
                LABEL_COM_Status.Text = "Getrennt";
                COM_TB_STATUS.Text = "???";
                LABEL_COM_Status.BackColor = Color.FromArgb(255, 128, 128);
                BUT_COM_Trennen.Enabled = false;
                BUT_COM_Verbinden.Enabled = true;
                COMBO_COM_Schnittstelle.Enabled = true;
                Timer1.Enabled = false;
            }
               
         }

        private void BUT_COM_Trennen_Click(object sender, EventArgs e)
        {
            if(SerPort1.IsOpen)
                {
                //!!! TODO: Absage ans Steuergerätsenden
                SerPort1.Close();
                }
                
            LABEL_COM_Status.Text = "Getrennt";
            COM_TB_STATUS.Text = "???";
            LABEL_COM_Status.BackColor = Color.FromArgb(255, 128, 128);
            BUT_COM_Trennen.Enabled = false;
            BUT_COM_Verbinden.Enabled = true;
            COMBO_COM_Schnittstelle.Enabled = true;
            Timer1.Enabled = false;
        }

        private void SerPort1_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            try { SerPort1.Close(); }
            catch (Exception) { }
            
            LABEL_COM_Status.Text = "Getrennt";
            COM_TB_STATUS.Text = "???";
            LABEL_COM_Status.BackColor = Color.FromArgb(255, 128, 128);
            BUT_COM_Trennen.Enabled = false;
            BUT_COM_Verbinden.Enabled = true;
            Timer1.Enabled = false;
        }

       
        

        private void SerPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            byte[] RxData = new byte[SerPort1.BytesToRead]; //Datenarray zum empfangen erstellen
            
            CIBEXCom.ResetPaket(ref USB_RxPaket);           //Datenpaket löschen

            if (SerPort1.BytesToRead == 14)
                {
                try
                    {
                    SerPort1.Read(RxData, 0, SerPort1.BytesToRead); //Daten aus Empfangspuffer holen
                    USB_RxPaket.Command = RxData[0]; //Statusbyte
                    for (int i = 0; i < USB_RxPaket.Data.Length; i++) USB_RxPaket.Data[i] = (ushort)(RxData[(i * 2) + 2] << 8 | RxData[(i * 2) + 3]);

                    MANUELL_SLIDER_Steps2.Value = USB_RxPaket.Data[0];
                    MANUELL_TB_ISTPOS.Text = USB_RxPaket.Data[0].ToString();
                    AUTO_TB_REPEAT2.Text = USB_RxPaket.Data[2].ToString();
                    AUTO_TB_REPEAT4.Text = USB_RxPaket.Data[4].ToString();
                    AUTO_TB_REPEAT5.Text = USB_RxPaket.Data[5].ToString();
                    COM_TB_STATUS.Text = USB_RxPaket.Command.ToString();
                    }
                catch (Exception) { }
                }

            try {
                SerPort1.DiscardInBuffer(); //Eingangspuffer löschen
                }
            catch (Exception) { }


            switch (USB_RxPaket.Command)
                {
                case STAT_OFFLINE:
                    COM_TB_STATUS.Text = "???";
                    break;
                case STAT_STANDBY:
                    COM_TB_STATUS.Text = "STANDBY";
                    BUT_AUTO_Start.Enabled = true;
                    BUT_AUTO_Stop.Enabled = false;
                    NUM_MANUELL_Steps.Enabled = true;
                    NUM_MANUELL_Speed.Enabled = true;
                    BUT_MANUELL_Nullen.Enabled = true;
                    BUT_MANUELL_RESET.Enabled = true;
                    break;
                case STAT_AUTO_RUN:
                    COM_TB_STATUS.Text = "Automatik läuft";
                    BUT_AUTO_Start.Enabled = false;
                    BUT_AUTO_Stop.Enabled = true;
                    NUM_MANUELL_Steps.Enabled = false;
                    NUM_MANUELL_Speed.Enabled = false;
                    BUT_MANUELL_Nullen.Enabled = false;
                    BUT_MANUELL_RESET.Enabled = false;
                    break;
                //case STAT_AUTO_PAUSE:
                //    BUT_AUTO_Start.Enabled = false;
                //    COM_TB_STATUS.Text = "Automatik pausiert";
                //    break;
                case STAT_MANU_RUN:
                    COM_TB_STATUS.Text = "Manuelle Bewegung";
                    BUT_AUTO_Start.Enabled = false;
                    BUT_AUTO_Stop.Enabled = false;
                    NUM_MANUELL_Steps.Enabled = false;
                    NUM_MANUELL_Speed.Enabled = false;
                    BUT_MANUELL_Nullen.Enabled = false;
                    BUT_MANUELL_RESET.Enabled = false;
                    break;
                case STAT_NULLEN:
                    COM_TB_STATUS.Text = "Nullen";
                    BUT_AUTO_Start.Enabled = false;
                    BUT_AUTO_Stop.Enabled = false;
                    NUM_MANUELL_Steps.Enabled = false;
                    NUM_MANUELL_Speed.Enabled = false;
                    BUT_MANUELL_Nullen.Enabled = false;
                    BUT_MANUELL_RESET.Enabled = false;
                    break;
                case STAT_ERROR:
                    COM_TB_STATUS.Text = "Fehler!!!";
                    BUT_AUTO_Start.Enabled = true;
                    BUT_AUTO_Stop.Enabled = false;
                    NUM_MANUELL_Steps.Enabled = true;
                    NUM_MANUELL_Speed.Enabled = true;
                    BUT_MANUELL_Nullen.Enabled = true;
                    BUT_MANUELL_RESET.Enabled = true;
                    break;
            }

        }

        private void Update_Schnittstellen(object sender, EventArgs e)
        {
            // Hole alle verfügbaren Com Schnittstellen
            COMBO_COM_Schnittstelle.Items.Clear();
            COMBO_COM_Schnittstelle.Items.AddRange(SerialPort.GetPortNames());
            if (COMBO_COM_Schnittstelle.Items.Count != 0) COMBO_COM_Schnittstelle.SelectedIndex = 0;
        }

        
        private void RADIO_AD_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
        
            switch (rb.Name)
            {
                case "RADIO_Rad":
                    USB_TxPaket.Unit = 0x00;
                    LABEL_AUTO_Warnung.BackColor = Color.Gold;
                    LABEL_AUTO_Warnung.ForeColor = Color.Black;

                    AUTO_NUM_STEP1.Maximum = 1700;
                    AUTO_NUM_STEP2.Maximum = 1700;
                    AUTO_NUM_STEP3.Maximum = 1700;
                    AUTO_NUM_STEP4.Maximum = 1700;
                    AUTO_NUM_ACCEL1.Maximum = 64000;
                    AUTO_NUM_ACCEL2.Maximum = 64000;
                    AUTO_NUM_ACCEL3.Maximum = 64000;
                    AUTO_NUM_ACCEL4.Maximum = 64000;
                    AUTO_NUM_SPEED1.Maximum = 16000;
                    AUTO_NUM_SPEED2.Maximum = 16000;
                    AUTO_NUM_SPEED3.Maximum = 16000;
                    AUTO_NUM_SPEED4.Maximum = 16000;
                    AUTO_NUM_DECEL1.Maximum = 64000;
                    AUTO_NUM_DECEL2.Maximum = 64000;
                    AUTO_NUM_DECEL3.Maximum = 64000;
                    AUTO_NUM_DECEL4.Maximum = 64000;

                    AUTO_NUM_STEP1.Value = 1000;
                    AUTO_NUM_STEP2.Value = 0;
                    AUTO_NUM_STEP3.Value = 0;
                    AUTO_NUM_STEP4.Value = 0;
                    AUTO_NUM_ACCEL1.Value = 32000;
                    AUTO_NUM_ACCEL2.Value = 32000;
                    AUTO_NUM_ACCEL3.Value = 0;
                    AUTO_NUM_ACCEL4.Value = 0;
                    AUTO_NUM_SPEED1.Value = 3200;
                    AUTO_NUM_SPEED2.Value = 3200;
                    AUTO_NUM_SPEED3.Value = 0;
                    AUTO_NUM_SPEED4.Value = 0;
                    AUTO_NUM_DECEL1.Value = 32000;
                    AUTO_NUM_DECEL2.Value = 32000;
                    AUTO_NUM_DECEL3.Value = 0;
                    AUTO_NUM_DECEL4.Value = 0;
                    break;

                case "RADIO_Steps":
                    USB_TxPaket.Unit = 0x01;
                    LABEL_AUTO_Warnung.BackColor = Color.LightYellow; //.FromArgb(255, 255, 192);
                    LABEL_AUTO_Warnung.ForeColor = Color.Gray;

                    AUTO_NUM_STEP1.Maximum = 1700;
                    AUTO_NUM_STEP2.Maximum = 1700;
                    AUTO_NUM_STEP3.Maximum = 1700;
                    AUTO_NUM_STEP4.Maximum = 1700;
                    AUTO_NUM_ACCEL1.Maximum = 20000;
                    AUTO_NUM_ACCEL2.Maximum = 20000;
                    AUTO_NUM_ACCEL3.Maximum = 20000;
                    AUTO_NUM_ACCEL4.Maximum = 20000;
                    AUTO_NUM_SPEED1.Maximum = 5000;
                    AUTO_NUM_SPEED2.Maximum = 5000;
                    AUTO_NUM_SPEED3.Maximum = 5000;
                    AUTO_NUM_SPEED4.Maximum = 5000;
                    AUTO_NUM_DECEL1.Maximum = 20000;
                    AUTO_NUM_DECEL2.Maximum = 20000;
                    AUTO_NUM_DECEL3.Maximum = 20000;
                    AUTO_NUM_DECEL4.Maximum = 20000;

                    AUTO_NUM_STEP1.Value = 1000;
                    AUTO_NUM_STEP2.Value = 0;
                    AUTO_NUM_STEP3.Value = 0;
                    AUTO_NUM_STEP4.Value = 0;
                    AUTO_NUM_ACCEL1.Value = 10000;
                    AUTO_NUM_ACCEL2.Value = 10000;
                    AUTO_NUM_ACCEL3.Value = 0;
                    AUTO_NUM_ACCEL4.Value = 0;
                    AUTO_NUM_SPEED1.Value = 1000;
                    AUTO_NUM_SPEED2.Value = 1000;
                    AUTO_NUM_SPEED3.Value = 0;
                    AUTO_NUM_SPEED4.Value = 0;
                    AUTO_NUM_DECEL1.Value = 10000;
                    AUTO_NUM_DECEL2.Value = 10000;
                    AUTO_NUM_DECEL3.Value = 0;
                    AUTO_NUM_DECEL4.Value = 0;
                    break;

                case "RADIO_mm":
                    USB_TxPaket.Unit = 0x02;
                    LABEL_AUTO_Warnung.BackColor = Color.Gold;
                    LABEL_AUTO_Warnung.ForeColor = Color.Black;

                    AUTO_NUM_STEP1.Maximum = 460;
                    AUTO_NUM_STEP2.Maximum = 460;
                    AUTO_NUM_STEP3.Maximum = 460;
                    AUTO_NUM_STEP4.Maximum = 460;
                    AUTO_NUM_ACCEL1.Maximum = 5400;
                    AUTO_NUM_ACCEL2.Maximum = 5400;
                    AUTO_NUM_ACCEL3.Maximum = 5400;
                    AUTO_NUM_ACCEL4.Maximum = 5400;
                    AUTO_NUM_SPEED1.Maximum = 1350;
                    AUTO_NUM_SPEED2.Maximum = 1350;
                    AUTO_NUM_SPEED3.Maximum = 1350;
                    AUTO_NUM_SPEED4.Maximum = 1350;
                    AUTO_NUM_DECEL1.Maximum = 5400;
                    AUTO_NUM_DECEL2.Maximum = 5400;
                    AUTO_NUM_DECEL3.Maximum = 5400;
                    AUTO_NUM_DECEL4.Maximum = 5400;

                    AUTO_NUM_STEP1.Value = 300;
                    AUTO_NUM_STEP2.Value = 0;
                    AUTO_NUM_STEP3.Value = 0;
                    AUTO_NUM_STEP4.Value = 0;
                    AUTO_NUM_ACCEL1.Value = 2700;
                    AUTO_NUM_ACCEL2.Value = 2700;
                    AUTO_NUM_ACCEL3.Value = 0;
                    AUTO_NUM_ACCEL4.Value = 0;
                    AUTO_NUM_SPEED1.Value = 1000;
                    AUTO_NUM_SPEED2.Value = 1000;
                    AUTO_NUM_SPEED3.Value = 0;
                    AUTO_NUM_SPEED4.Value = 0;
                    AUTO_NUM_DECEL1.Value = 2700;
                    AUTO_NUM_DECEL2.Value = 2700;
                    AUTO_NUM_DECEL3.Value = 0;
                    AUTO_NUM_DECEL4.Value = 0;
                    break;

                case "RADIO_mg":
                    USB_TxPaket.Unit = 0x03;
                    LABEL_AUTO_Warnung.BackColor = Color.Gold;
                    LABEL_AUTO_Warnung.ForeColor = Color.Black;

                    AUTO_NUM_STEP1.Maximum = 460;
                    AUTO_NUM_STEP2.Maximum = 460;
                    AUTO_NUM_STEP3.Maximum = 460;
                    AUTO_NUM_STEP4.Maximum = 460;
                    AUTO_NUM_ACCEL1.Maximum = 550;
                    AUTO_NUM_ACCEL2.Maximum = 550;
                    AUTO_NUM_ACCEL3.Maximum = 550;
                    AUTO_NUM_ACCEL4.Maximum = 550;
                    AUTO_NUM_SPEED1.Maximum = 1350;
                    AUTO_NUM_SPEED2.Maximum = 1350;
                    AUTO_NUM_SPEED3.Maximum = 1350;
                    AUTO_NUM_SPEED4.Maximum = 1350;
                    AUTO_NUM_DECEL1.Maximum = 550;
                    AUTO_NUM_DECEL2.Maximum = 550;
                    AUTO_NUM_DECEL3.Maximum = 550;
                    AUTO_NUM_DECEL4.Maximum = 550;

                    AUTO_NUM_STEP1.Value = 300;
                    AUTO_NUM_STEP2.Value = 0;
                    AUTO_NUM_STEP3.Value = 0;
                    AUTO_NUM_STEP4.Value = 0;
                    AUTO_NUM_ACCEL1.Value = 300;
                    AUTO_NUM_ACCEL2.Value = 300;
                    AUTO_NUM_ACCEL3.Value = 0;
                    AUTO_NUM_ACCEL4.Value = 0;
                    AUTO_NUM_SPEED1.Value = 1000;
                    AUTO_NUM_SPEED2.Value = 1000;
                    AUTO_NUM_SPEED3.Value = 0;
                    AUTO_NUM_SPEED4.Value = 0;
                    AUTO_NUM_DECEL1.Value = 300;
                    AUTO_NUM_DECEL2.Value = 300;
                    AUTO_NUM_DECEL3.Value = 0;
                    AUTO_NUM_DECEL4.Value = 0;
                    break;
            }
        }


        private void Timer1_Tick(object sender, EventArgs e)
        {
            CIBEXCom.ResetPaket(ref USB_TxPaket);
            USB_TxPaket.Command = CMD_REQUEST_STATUS;
            CIBEXCom.Send(ref USB_TxPaket, ref SerPort1);
        }


        private void BUT_AUTO_Start_Click(object sender, EventArgs e)
            {
            BUT_AUTO_Start.Enabled = false;
            BUT_AUTO_Stop.Enabled = true;
            NUM_MANUELL_Steps.Enabled = false;
            NUM_MANUELL_Speed.Enabled = false;
            BUT_MANUELL_Nullen.Enabled = false;
            BUT_MANUELL_RESET.Enabled = false;

            CIBEXCom.ResetPaket(ref USB_TxPaket);
            USB_TxPaket.Command  = CMD_AUTO_START;
            USB_TxPaket.Data[0]  = Convert.ToUInt16(AUTO_NUM_STEP1.Value);
            USB_TxPaket.Data[1]  = Convert.ToUInt16(AUTO_NUM_SPEED1.Value);
            USB_TxPaket.Data[2]  = Convert.ToUInt16(AUTO_NUM_ACCEL1.Value);
            USB_TxPaket.Data[3]  = Convert.ToUInt16(AUTO_NUM_DECEL1.Value);
            USB_TxPaket.Data[4]  = Convert.ToUInt16(AUTO_NUM_DELAY1.Value);
            USB_TxPaket.Data[5]  = Convert.ToUInt16(0);

            USB_TxPaket.Data[6]  = Convert.ToUInt16(AUTO_NUM_STEP2.Value);
            USB_TxPaket.Data[7]  = Convert.ToUInt16(AUTO_NUM_SPEED2.Value);
            USB_TxPaket.Data[8]  = Convert.ToUInt16(AUTO_NUM_ACCEL2.Value);
            USB_TxPaket.Data[9]  = Convert.ToUInt16(AUTO_NUM_DECEL2.Value);
            USB_TxPaket.Data[10] = Convert.ToUInt16(AUTO_NUM_DELAY2.Value);
            USB_TxPaket.Data[11] = Convert.ToUInt16(AUTO_NUM_REPEAT2.Value+1);

            USB_TxPaket.Data[12] = Convert.ToUInt16(AUTO_NUM_STEP3.Value);
            USB_TxPaket.Data[13] = Convert.ToUInt16(AUTO_NUM_SPEED3.Value);
            USB_TxPaket.Data[14] = Convert.ToUInt16(AUTO_NUM_ACCEL3.Value);
            USB_TxPaket.Data[15] = Convert.ToUInt16(AUTO_NUM_DECEL3.Value);
            USB_TxPaket.Data[16] = Convert.ToUInt16(AUTO_NUM_DELAY3.Value);
            USB_TxPaket.Data[17] = Convert.ToUInt16(0);

            USB_TxPaket.Data[18] = Convert.ToUInt16(AUTO_NUM_STEP4.Value);
            USB_TxPaket.Data[19] = Convert.ToUInt16(AUTO_NUM_SPEED4.Value);
            USB_TxPaket.Data[20] = Convert.ToUInt16(AUTO_NUM_ACCEL4.Value);
            USB_TxPaket.Data[21] = Convert.ToUInt16(AUTO_NUM_DECEL4.Value);
            USB_TxPaket.Data[22] = Convert.ToUInt16(AUTO_NUM_DELAY4.Value);
            USB_TxPaket.Data[23] = Convert.ToUInt16(AUTO_NUM_REPEAT4.Value+1);

            USB_TxPaket.Data[24] = Convert.ToUInt16(AUTO_NUM_DELAY5.Value);
            USB_TxPaket.Data[25] = Convert.ToUInt16(AUTO_NUM_REPEAT5.Value+1);

            CIBEXCom.Send(ref USB_TxPaket, ref SerPort1);

        }

        private void BUT_AUTO_Stop_Click(object sender, EventArgs e)
            {
            CIBEXCom.ResetPaket(ref USB_TxPaket);
            USB_TxPaket.Command = CMD_AUTO_STOP;
            }

        private void BUT_AUTO_Pause_Click(object sender, EventArgs e)
        {
         
            if (BUT_AUTO_Pause.Text == "Pause")
                {
                BUT_AUTO_Pause.Text = "Fortsetzen";

                }
            else
                {
                BUT_AUTO_Pause.Text = "Pause";

                }

        }

        //### MANUELL
        private void NUM_MANUELL_Steps_ValueChanged(object sender, EventArgs e)
        {
            CIBEXCom.ResetPaket(ref USB_TxPaket);
            USB_TxPaket.Command = CMD_MANU_NEW_POS;
            USB_TxPaket.Data[0] = Convert.ToUInt16(NUM_MANUELL_Steps.Value);
            USB_TxPaket.Data[1] = Convert.ToUInt16(NUM_MANUELL_Speed.Value);
            USB_TxPaket.Data[2] = Convert.ToUInt16(NUM_MANUELL_AccDec.Value);
            CIBEXCom.Send(ref USB_TxPaket, ref SerPort1);
        }

        private void BUT_MANUELL_Nullen_Click(object sender, EventArgs e)
        {
            CIBEXCom.ResetPaket(ref USB_TxPaket);

            USB_TxPaket.Command = CMD_MANU_NULL_POS;
            CIBEXCom.Send(ref USB_TxPaket, ref SerPort1);
        }

        private void BUT_MANUELL_RESET_Click(object sender, EventArgs e)
        {
            CIBEXCom.ResetPaket(ref USB_TxPaket);

            USB_TxPaket.Command = CMD_UNIT_RESET;
            CIBEXCom.Send(ref USB_TxPaket, ref SerPort1);
        }
    }
}
