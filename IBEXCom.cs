﻿using System;
using System.CodeDom;
using System.IO.Ports;

namespace LinearAntrieb_PCProg
{
    

    public struct t_Paket
    {
        public byte Command;
        public byte Unit;
        public ushort[] Data;
        //public byte DataLength;
        
 
        // Konstruktor mit Initialisierung
        public t_Paket(ushort ArrayLength)
        {
            Command = 0x01;
            Unit = 0x01;
            Data    = new ushort[ArrayLength];
            //DataLength = 0;
        }
    }

    

    public class IBEXCom
    {
        public void ResetPaket(ref t_Paket Paket)
            {
            for (int i = 0; i < Paket.Data.Length; i++) Paket.Data[i] = 0;  // Zurücksetzen
            //Paket.DataLength = 0;
            Paket.Command = 0x01;
            }

        public int Send(ref t_Paket Paket, ref SerialPort ComPort)
            {
            byte[] TxData = new byte[(Paket.Data.Length*2) + 2]; //Datenpaket zum senden erstellen
            
            TxData[0] = Paket.Command;
            TxData[1] = Paket.Unit;
            for(int i=0; i<Paket.Data.Length; i++)
                {
                TxData[(i*2)+2] = (byte)(Paket.Data[i] >> 8);
                TxData[(i*2)+3] = (byte)(Paket.Data[i] & 0xFF);
                }

            try
                {
                ComPort.Write(TxData, 0, TxData.Length); // Senden
                }
            catch (Exception)
                {
                return -1;
                }

            return 1;
            }
        
    }
}

