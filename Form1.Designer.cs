﻿namespace LinearAntrieb_PCProg
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.AUTO_TB_REPEAT5 = new System.Windows.Forms.TextBox();
            this.SerPort1 = new System.IO.Ports.SerialPort(this.components);
            this.BUT_MANUELL_Nullen = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.NUM_MANUELL_AccDec = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.MANUELL_TB_ISTPOS = new System.Windows.Forms.TextBox();
            this.NUM_MANUELL_Steps = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.NUM_MANUELL_Speed = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.MANUELL_SLIDER_Steps2 = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.AUTO_NUM_REPEAT4 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_REPEAT2 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_REPEAT5 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_DELAY4 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_DELAY3 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_DELAY2 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_DECEL4 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_SPEED4 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_ACCEL4 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_DECEL3 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_SPEED3 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_ACCEL3 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_DECEL2 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_SPEED2 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_ACCEL2 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_STEP4 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_STEP3 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_STEP2 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_DECEL1 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_SPEED1 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_DELAY1 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_ACCEL1 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_STEP1 = new System.Windows.Forms.NumericUpDown();
            this.AUTO_NUM_DELAY5 = new System.Windows.Forms.NumericUpDown();
            this.label30 = new System.Windows.Forms.Label();
            this.AUTO_TB_REPEAT4 = new System.Windows.Forms.TextBox();
            this.AUTO_TB_REPEAT2 = new System.Windows.Forms.TextBox();
            this.BUT_AUTO_Pause = new System.Windows.Forms.Button();
            this.BUT_AUTO_Stop = new System.Windows.Forms.Button();
            this.BUT_AUTO_Start = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.LABEL_AUTO_Fehler = new System.Windows.Forms.Label();
            this.LABEL_AUTO_Warnung = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.BUT_MANUELL_RESET = new System.Windows.Forms.Button();
            this.COM_TB_STATUS = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.RADIO_mg = new System.Windows.Forms.RadioButton();
            this.label41 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.RADIO_mm = new System.Windows.Forms.RadioButton();
            this.label14 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.RADIO_Steps = new System.Windows.Forms.RadioButton();
            this.label24 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.RADIO_Rad = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.TB_Mech_VW = new System.Windows.Forms.TextBox();
            this.TB_Mech_Motor = new System.Windows.Forms.TextBox();
            this.TB_Mech_SL = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.LABEL_COM_Status = new System.Windows.Forms.Label();
            this.COMBO_COM_Schnittstelle = new System.Windows.Forms.ComboBox();
            this.BUT_COM_Trennen = new System.Windows.Forms.Button();
            this.BUT_COM_Verbinden = new System.Windows.Forms.Button();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_MANUELL_AccDec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_MANUELL_Steps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_MANUELL_Speed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MANUELL_SLIDER_Steps2)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_REPEAT4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_REPEAT2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_REPEAT5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_DELAY4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_DELAY3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_DELAY2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_DECEL4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_SPEED4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_ACCEL4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_DECEL3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_SPEED3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_ACCEL3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_DECEL2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_SPEED2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_ACCEL2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_STEP4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_STEP3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_STEP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_DECEL1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_SPEED1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_DELAY1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_ACCEL1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_STEP1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_DELAY5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // AUTO_TB_REPEAT5
            // 
            this.AUTO_TB_REPEAT5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.AUTO_TB_REPEAT5.Enabled = false;
            this.AUTO_TB_REPEAT5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.AUTO_TB_REPEAT5.Location = new System.Drawing.Point(444, 297);
            this.AUTO_TB_REPEAT5.Name = "AUTO_TB_REPEAT5";
            this.AUTO_TB_REPEAT5.Size = new System.Drawing.Size(44, 20);
            this.AUTO_TB_REPEAT5.TabIndex = 11;
            this.AUTO_TB_REPEAT5.TabStop = false;
            this.AUTO_TB_REPEAT5.Text = "0";
            this.AUTO_TB_REPEAT5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // SerPort1
            // 
            this.SerPort1.PortName = "COM5";
            this.SerPort1.ErrorReceived += new System.IO.Ports.SerialErrorReceivedEventHandler(this.SerPort1_ErrorReceived);
            this.SerPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.SerPort1_DataReceived);
            // 
            // BUT_MANUELL_Nullen
            // 
            this.BUT_MANUELL_Nullen.Location = new System.Drawing.Point(11, 49);
            this.BUT_MANUELL_Nullen.Name = "BUT_MANUELL_Nullen";
            this.BUT_MANUELL_Nullen.Size = new System.Drawing.Size(88, 38);
            this.BUT_MANUELL_Nullen.TabIndex = 7;
            this.BUT_MANUELL_Nullen.Text = "Schlitten \"Nullen\"";
            this.BUT_MANUELL_Nullen.UseVisualStyleBackColor = true;
            this.BUT_MANUELL_Nullen.Click += new System.EventHandler(this.BUT_MANUELL_Nullen_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.NUM_MANUELL_AccDec);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.MANUELL_TB_ISTPOS);
            this.groupBox1.Controls.Add(this.NUM_MANUELL_Steps);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.NUM_MANUELL_Speed);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label37);
            this.groupBox1.Controls.Add(this.MANUELL_SLIDER_Steps2);
            this.groupBox1.Location = new System.Drawing.Point(246, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(125, 611);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Manuelle Bedienung";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.SystemColors.Control;
            this.label26.Location = new System.Drawing.Point(5, 574);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(51, 13);
            this.label26.TabIndex = 107;
            this.label26.Text = "Acc/Dec";
            // 
            // NUM_MANUELL_AccDec
            // 
            this.NUM_MANUELL_AccDec.Location = new System.Drawing.Point(59, 571);
            this.NUM_MANUELL_AccDec.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.NUM_MANUELL_AccDec.Name = "NUM_MANUELL_AccDec";
            this.NUM_MANUELL_AccDec.Size = new System.Drawing.Size(56, 20);
            this.NUM_MANUELL_AccDec.TabIndex = 106;
            this.NUM_MANUELL_AccDec.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NUM_MANUELL_AccDec.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            this.NUM_MANUELL_AccDec.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.SystemColors.Control;
            this.label15.Location = new System.Drawing.Point(12, 527);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(44, 13);
            this.label15.TabIndex = 105;
            this.label15.Text = "Position";
            // 
            // MANUELL_TB_ISTPOS
            // 
            this.MANUELL_TB_ISTPOS.BackColor = System.Drawing.SystemColors.Control;
            this.MANUELL_TB_ISTPOS.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MANUELL_TB_ISTPOS.Enabled = false;
            this.MANUELL_TB_ISTPOS.ForeColor = System.Drawing.SystemColors.GrayText;
            this.MANUELL_TB_ISTPOS.Location = new System.Drawing.Point(65, 30);
            this.MANUELL_TB_ISTPOS.Name = "MANUELL_TB_ISTPOS";
            this.MANUELL_TB_ISTPOS.Size = new System.Drawing.Size(48, 13);
            this.MANUELL_TB_ISTPOS.TabIndex = 97;
            this.MANUELL_TB_ISTPOS.TabStop = false;
            this.MANUELL_TB_ISTPOS.Text = "0";
            this.MANUELL_TB_ISTPOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // NUM_MANUELL_Steps
            // 
            this.NUM_MANUELL_Steps.Location = new System.Drawing.Point(59, 524);
            this.NUM_MANUELL_Steps.Maximum = new decimal(new int[] {
            1700,
            0,
            0,
            0});
            this.NUM_MANUELL_Steps.Name = "NUM_MANUELL_Steps";
            this.NUM_MANUELL_Steps.Size = new System.Drawing.Size(56, 20);
            this.NUM_MANUELL_Steps.TabIndex = 100;
            this.NUM_MANUELL_Steps.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NUM_MANUELL_Steps.ThousandsSeparator = true;
            this.NUM_MANUELL_Steps.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            this.NUM_MANUELL_Steps.ValueChanged += new System.EventHandler(this.NUM_MANUELL_Steps_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.SystemColors.Control;
            this.label11.Location = new System.Drawing.Point(12, 551);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 103;
            this.label11.Text = "Steps/s";
            // 
            // NUM_MANUELL_Speed
            // 
            this.NUM_MANUELL_Speed.Location = new System.Drawing.Point(59, 548);
            this.NUM_MANUELL_Speed.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.NUM_MANUELL_Speed.Name = "NUM_MANUELL_Speed";
            this.NUM_MANUELL_Speed.Size = new System.Drawing.Size(56, 20);
            this.NUM_MANUELL_Speed.TabIndex = 102;
            this.NUM_MANUELL_Speed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NUM_MANUELL_Speed.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            this.NUM_MANUELL_Speed.Value = new decimal(new int[] {
            1500,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.SystemColors.Control;
            this.label10.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label10.Location = new System.Drawing.Point(43, 101);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 101;
            this.label10.Text = "1500";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label7.Location = new System.Drawing.Point(43, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 98;
            this.label7.Text = "1700";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.Control;
            this.label6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label6.Location = new System.Drawing.Point(43, 234);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 82;
            this.label6.Text = "1000";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label5.Location = new System.Drawing.Point(49, 367);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 82;
            this.label5.Text = "500";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.SystemColors.Control;
            this.label37.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label37.Location = new System.Drawing.Point(61, 500);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(13, 13);
            this.label37.TabIndex = 82;
            this.label37.Text = "0";
            // 
            // MANUELL_SLIDER_Steps2
            // 
            this.MANUELL_SLIDER_Steps2.AutoSize = false;
            this.MANUELL_SLIDER_Steps2.BackColor = System.Drawing.SystemColors.Control;
            this.MANUELL_SLIDER_Steps2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MANUELL_SLIDER_Steps2.Enabled = false;
            this.MANUELL_SLIDER_Steps2.LargeChange = 10;
            this.MANUELL_SLIDER_Steps2.Location = new System.Drawing.Point(69, 41);
            this.MANUELL_SLIDER_Steps2.Maximum = 1700;
            this.MANUELL_SLIDER_Steps2.Name = "MANUELL_SLIDER_Steps2";
            this.MANUELL_SLIDER_Steps2.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.MANUELL_SLIDER_Steps2.Size = new System.Drawing.Size(35, 479);
            this.MANUELL_SLIDER_Steps2.SmallChange = 10;
            this.MANUELL_SLIDER_Steps2.TabIndex = 104;
            this.MANUELL_SLIDER_Steps2.TabStop = false;
            this.MANUELL_SLIDER_Steps2.TickFrequency = 100;
            this.MANUELL_SLIDER_Steps2.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label1.Location = new System.Drawing.Point(62, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Zielposition";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label2.Location = new System.Drawing.Point(203, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Speed";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox2.Controls.Add(this.AUTO_NUM_REPEAT4);
            this.groupBox2.Controls.Add(this.AUTO_NUM_REPEAT2);
            this.groupBox2.Controls.Add(this.AUTO_NUM_REPEAT5);
            this.groupBox2.Controls.Add(this.AUTO_NUM_DELAY4);
            this.groupBox2.Controls.Add(this.AUTO_NUM_DELAY3);
            this.groupBox2.Controls.Add(this.AUTO_NUM_DELAY2);
            this.groupBox2.Controls.Add(this.AUTO_NUM_DECEL4);
            this.groupBox2.Controls.Add(this.AUTO_NUM_SPEED4);
            this.groupBox2.Controls.Add(this.AUTO_NUM_ACCEL4);
            this.groupBox2.Controls.Add(this.AUTO_NUM_DECEL3);
            this.groupBox2.Controls.Add(this.AUTO_NUM_SPEED3);
            this.groupBox2.Controls.Add(this.AUTO_NUM_ACCEL3);
            this.groupBox2.Controls.Add(this.AUTO_NUM_DECEL2);
            this.groupBox2.Controls.Add(this.AUTO_NUM_SPEED2);
            this.groupBox2.Controls.Add(this.AUTO_NUM_ACCEL2);
            this.groupBox2.Controls.Add(this.AUTO_NUM_STEP4);
            this.groupBox2.Controls.Add(this.AUTO_NUM_STEP3);
            this.groupBox2.Controls.Add(this.AUTO_NUM_STEP2);
            this.groupBox2.Controls.Add(this.AUTO_NUM_DECEL1);
            this.groupBox2.Controls.Add(this.AUTO_NUM_SPEED1);
            this.groupBox2.Controls.Add(this.AUTO_NUM_DELAY1);
            this.groupBox2.Controls.Add(this.AUTO_NUM_ACCEL1);
            this.groupBox2.Controls.Add(this.AUTO_NUM_STEP1);
            this.groupBox2.Controls.Add(this.AUTO_NUM_DELAY5);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.AUTO_TB_REPEAT4);
            this.groupBox2.Controls.Add(this.AUTO_TB_REPEAT2);
            this.groupBox2.Controls.Add(this.AUTO_TB_REPEAT5);
            this.groupBox2.Controls.Add(this.BUT_AUTO_Pause);
            this.groupBox2.Controls.Add(this.BUT_AUTO_Stop);
            this.groupBox2.Controls.Add(this.BUT_AUTO_Start);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Location = new System.Drawing.Point(387, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(662, 611);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Automatik";
            // 
            // AUTO_NUM_REPEAT4
            // 
            this.AUTO_NUM_REPEAT4.Location = new System.Drawing.Point(333, 365);
            this.AUTO_NUM_REPEAT4.Maximum = new decimal(new int[] {
            65534,
            0,
            0,
            0});
            this.AUTO_NUM_REPEAT4.Name = "AUTO_NUM_REPEAT4";
            this.AUTO_NUM_REPEAT4.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_REPEAT4.TabIndex = 93;
            this.AUTO_NUM_REPEAT4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_REPEAT4.ThousandsSeparator = true;
            this.AUTO_NUM_REPEAT4.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            // 
            // AUTO_NUM_REPEAT2
            // 
            this.AUTO_NUM_REPEAT2.Location = new System.Drawing.Point(333, 163);
            this.AUTO_NUM_REPEAT2.Maximum = new decimal(new int[] {
            65534,
            0,
            0,
            0});
            this.AUTO_NUM_REPEAT2.Name = "AUTO_NUM_REPEAT2";
            this.AUTO_NUM_REPEAT2.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_REPEAT2.TabIndex = 93;
            this.AUTO_NUM_REPEAT2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_REPEAT2.ThousandsSeparator = true;
            this.AUTO_NUM_REPEAT2.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            this.AUTO_NUM_REPEAT2.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // AUTO_NUM_REPEAT5
            // 
            this.AUTO_NUM_REPEAT5.Location = new System.Drawing.Point(403, 275);
            this.AUTO_NUM_REPEAT5.Maximum = new decimal(new int[] {
            65534,
            0,
            0,
            0});
            this.AUTO_NUM_REPEAT5.Name = "AUTO_NUM_REPEAT5";
            this.AUTO_NUM_REPEAT5.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_REPEAT5.TabIndex = 93;
            this.AUTO_NUM_REPEAT5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_REPEAT5.ThousandsSeparator = true;
            this.AUTO_NUM_REPEAT5.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            this.AUTO_NUM_REPEAT5.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // AUTO_NUM_DELAY4
            // 
            this.AUTO_NUM_DELAY4.Location = new System.Drawing.Point(169, 439);
            this.AUTO_NUM_DELAY4.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.AUTO_NUM_DELAY4.Name = "AUTO_NUM_DELAY4";
            this.AUTO_NUM_DELAY4.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_DELAY4.TabIndex = 92;
            this.AUTO_NUM_DELAY4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_DELAY4.ThousandsSeparator = true;
            // 
            // AUTO_NUM_DELAY3
            // 
            this.AUTO_NUM_DELAY3.Location = new System.Drawing.Point(169, 338);
            this.AUTO_NUM_DELAY3.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.AUTO_NUM_DELAY3.Name = "AUTO_NUM_DELAY3";
            this.AUTO_NUM_DELAY3.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_DELAY3.TabIndex = 92;
            this.AUTO_NUM_DELAY3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_DELAY3.ThousandsSeparator = true;
            // 
            // AUTO_NUM_DELAY2
            // 
            this.AUTO_NUM_DELAY2.Location = new System.Drawing.Point(169, 238);
            this.AUTO_NUM_DELAY2.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.AUTO_NUM_DELAY2.Name = "AUTO_NUM_DELAY2";
            this.AUTO_NUM_DELAY2.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_DELAY2.TabIndex = 91;
            this.AUTO_NUM_DELAY2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_DELAY2.ThousandsSeparator = true;
            this.AUTO_NUM_DELAY2.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // AUTO_NUM_DECEL4
            // 
            this.AUTO_NUM_DECEL4.Location = new System.Drawing.Point(257, 398);
            this.AUTO_NUM_DECEL4.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.AUTO_NUM_DECEL4.Name = "AUTO_NUM_DECEL4";
            this.AUTO_NUM_DECEL4.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_DECEL4.TabIndex = 88;
            this.AUTO_NUM_DECEL4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_DECEL4.ThousandsSeparator = true;
            // 
            // AUTO_NUM_SPEED4
            // 
            this.AUTO_NUM_SPEED4.Location = new System.Drawing.Point(193, 398);
            this.AUTO_NUM_SPEED4.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.AUTO_NUM_SPEED4.Name = "AUTO_NUM_SPEED4";
            this.AUTO_NUM_SPEED4.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_SPEED4.TabIndex = 88;
            this.AUTO_NUM_SPEED4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_SPEED4.ThousandsSeparator = true;
            // 
            // AUTO_NUM_ACCEL4
            // 
            this.AUTO_NUM_ACCEL4.Location = new System.Drawing.Point(129, 398);
            this.AUTO_NUM_ACCEL4.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.AUTO_NUM_ACCEL4.Name = "AUTO_NUM_ACCEL4";
            this.AUTO_NUM_ACCEL4.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_ACCEL4.TabIndex = 88;
            this.AUTO_NUM_ACCEL4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_ACCEL4.ThousandsSeparator = true;
            // 
            // AUTO_NUM_DECEL3
            // 
            this.AUTO_NUM_DECEL3.Location = new System.Drawing.Point(257, 298);
            this.AUTO_NUM_DECEL3.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.AUTO_NUM_DECEL3.Name = "AUTO_NUM_DECEL3";
            this.AUTO_NUM_DECEL3.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_DECEL3.TabIndex = 89;
            this.AUTO_NUM_DECEL3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_DECEL3.ThousandsSeparator = true;
            // 
            // AUTO_NUM_SPEED3
            // 
            this.AUTO_NUM_SPEED3.Location = new System.Drawing.Point(193, 298);
            this.AUTO_NUM_SPEED3.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.AUTO_NUM_SPEED3.Name = "AUTO_NUM_SPEED3";
            this.AUTO_NUM_SPEED3.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_SPEED3.TabIndex = 89;
            this.AUTO_NUM_SPEED3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_SPEED3.ThousandsSeparator = true;
            // 
            // AUTO_NUM_ACCEL3
            // 
            this.AUTO_NUM_ACCEL3.Location = new System.Drawing.Point(129, 298);
            this.AUTO_NUM_ACCEL3.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.AUTO_NUM_ACCEL3.Name = "AUTO_NUM_ACCEL3";
            this.AUTO_NUM_ACCEL3.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_ACCEL3.TabIndex = 89;
            this.AUTO_NUM_ACCEL3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_ACCEL3.ThousandsSeparator = true;
            // 
            // AUTO_NUM_DECEL2
            // 
            this.AUTO_NUM_DECEL2.Location = new System.Drawing.Point(257, 198);
            this.AUTO_NUM_DECEL2.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.AUTO_NUM_DECEL2.Name = "AUTO_NUM_DECEL2";
            this.AUTO_NUM_DECEL2.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_DECEL2.TabIndex = 90;
            this.AUTO_NUM_DECEL2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_DECEL2.ThousandsSeparator = true;
            this.AUTO_NUM_DECEL2.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            // 
            // AUTO_NUM_SPEED2
            // 
            this.AUTO_NUM_SPEED2.Location = new System.Drawing.Point(193, 198);
            this.AUTO_NUM_SPEED2.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.AUTO_NUM_SPEED2.Name = "AUTO_NUM_SPEED2";
            this.AUTO_NUM_SPEED2.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_SPEED2.TabIndex = 90;
            this.AUTO_NUM_SPEED2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_SPEED2.ThousandsSeparator = true;
            this.AUTO_NUM_SPEED2.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // AUTO_NUM_ACCEL2
            // 
            this.AUTO_NUM_ACCEL2.Location = new System.Drawing.Point(129, 198);
            this.AUTO_NUM_ACCEL2.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.AUTO_NUM_ACCEL2.Name = "AUTO_NUM_ACCEL2";
            this.AUTO_NUM_ACCEL2.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_ACCEL2.TabIndex = 90;
            this.AUTO_NUM_ACCEL2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_ACCEL2.ThousandsSeparator = true;
            this.AUTO_NUM_ACCEL2.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            // 
            // AUTO_NUM_STEP4
            // 
            this.AUTO_NUM_STEP4.Location = new System.Drawing.Point(65, 398);
            this.AUTO_NUM_STEP4.Maximum = new decimal(new int[] {
            1700,
            0,
            0,
            0});
            this.AUTO_NUM_STEP4.Name = "AUTO_NUM_STEP4";
            this.AUTO_NUM_STEP4.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_STEP4.TabIndex = 87;
            this.AUTO_NUM_STEP4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_STEP4.ThousandsSeparator = true;
            // 
            // AUTO_NUM_STEP3
            // 
            this.AUTO_NUM_STEP3.Location = new System.Drawing.Point(65, 298);
            this.AUTO_NUM_STEP3.Maximum = new decimal(new int[] {
            1700,
            0,
            0,
            0});
            this.AUTO_NUM_STEP3.Name = "AUTO_NUM_STEP3";
            this.AUTO_NUM_STEP3.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_STEP3.TabIndex = 87;
            this.AUTO_NUM_STEP3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_STEP3.ThousandsSeparator = true;
            // 
            // AUTO_NUM_STEP2
            // 
            this.AUTO_NUM_STEP2.Location = new System.Drawing.Point(65, 198);
            this.AUTO_NUM_STEP2.Maximum = new decimal(new int[] {
            1700,
            0,
            0,
            0});
            this.AUTO_NUM_STEP2.Name = "AUTO_NUM_STEP2";
            this.AUTO_NUM_STEP2.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_STEP2.TabIndex = 87;
            this.AUTO_NUM_STEP2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_STEP2.ThousandsSeparator = true;
            // 
            // AUTO_NUM_DECEL1
            // 
            this.AUTO_NUM_DECEL1.Location = new System.Drawing.Point(257, 99);
            this.AUTO_NUM_DECEL1.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.AUTO_NUM_DECEL1.Name = "AUTO_NUM_DECEL1";
            this.AUTO_NUM_DECEL1.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_DECEL1.TabIndex = 86;
            this.AUTO_NUM_DECEL1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_DECEL1.ThousandsSeparator = true;
            this.AUTO_NUM_DECEL1.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            // 
            // AUTO_NUM_SPEED1
            // 
            this.AUTO_NUM_SPEED1.Location = new System.Drawing.Point(193, 99);
            this.AUTO_NUM_SPEED1.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.AUTO_NUM_SPEED1.Name = "AUTO_NUM_SPEED1";
            this.AUTO_NUM_SPEED1.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_SPEED1.TabIndex = 85;
            this.AUTO_NUM_SPEED1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_SPEED1.ThousandsSeparator = true;
            this.AUTO_NUM_SPEED1.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // AUTO_NUM_DELAY1
            // 
            this.AUTO_NUM_DELAY1.Location = new System.Drawing.Point(169, 138);
            this.AUTO_NUM_DELAY1.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.AUTO_NUM_DELAY1.Name = "AUTO_NUM_DELAY1";
            this.AUTO_NUM_DELAY1.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_DELAY1.TabIndex = 84;
            this.AUTO_NUM_DELAY1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_DELAY1.ThousandsSeparator = true;
            this.AUTO_NUM_DELAY1.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // AUTO_NUM_ACCEL1
            // 
            this.AUTO_NUM_ACCEL1.Location = new System.Drawing.Point(129, 99);
            this.AUTO_NUM_ACCEL1.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.AUTO_NUM_ACCEL1.Name = "AUTO_NUM_ACCEL1";
            this.AUTO_NUM_ACCEL1.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_ACCEL1.TabIndex = 84;
            this.AUTO_NUM_ACCEL1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_ACCEL1.ThousandsSeparator = true;
            this.AUTO_NUM_ACCEL1.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            // 
            // AUTO_NUM_STEP1
            // 
            this.AUTO_NUM_STEP1.Location = new System.Drawing.Point(65, 99);
            this.AUTO_NUM_STEP1.Maximum = new decimal(new int[] {
            1700,
            0,
            0,
            0});
            this.AUTO_NUM_STEP1.Name = "AUTO_NUM_STEP1";
            this.AUTO_NUM_STEP1.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_STEP1.TabIndex = 83;
            this.AUTO_NUM_STEP1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_STEP1.ThousandsSeparator = true;
            this.AUTO_NUM_STEP1.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // AUTO_NUM_DELAY5
            // 
            this.AUTO_NUM_DELAY5.Location = new System.Drawing.Point(168, 498);
            this.AUTO_NUM_DELAY5.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.AUTO_NUM_DELAY5.Name = "AUTO_NUM_DELAY5";
            this.AUTO_NUM_DELAY5.Size = new System.Drawing.Size(58, 20);
            this.AUTO_NUM_DELAY5.TabIndex = 82;
            this.AUTO_NUM_DELAY5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AUTO_NUM_DELAY5.ThousandsSeparator = true;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label30.Location = new System.Drawing.Point(112, 442);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(56, 13);
            this.label30.TabIndex = 81;
            this.label30.Text = "Delay [ms]";
            // 
            // AUTO_TB_REPEAT4
            // 
            this.AUTO_TB_REPEAT4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.AUTO_TB_REPEAT4.Enabled = false;
            this.AUTO_TB_REPEAT4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.AUTO_TB_REPEAT4.Location = new System.Drawing.Point(373, 387);
            this.AUTO_TB_REPEAT4.Name = "AUTO_TB_REPEAT4";
            this.AUTO_TB_REPEAT4.Size = new System.Drawing.Size(44, 20);
            this.AUTO_TB_REPEAT4.TabIndex = 79;
            this.AUTO_TB_REPEAT4.TabStop = false;
            this.AUTO_TB_REPEAT4.Text = "0";
            this.AUTO_TB_REPEAT4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // AUTO_TB_REPEAT2
            // 
            this.AUTO_TB_REPEAT2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.AUTO_TB_REPEAT2.Enabled = false;
            this.AUTO_TB_REPEAT2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.AUTO_TB_REPEAT2.Location = new System.Drawing.Point(373, 185);
            this.AUTO_TB_REPEAT2.Name = "AUTO_TB_REPEAT2";
            this.AUTO_TB_REPEAT2.Size = new System.Drawing.Size(44, 20);
            this.AUTO_TB_REPEAT2.TabIndex = 77;
            this.AUTO_TB_REPEAT2.TabStop = false;
            this.AUTO_TB_REPEAT2.Text = "0";
            this.AUTO_TB_REPEAT2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BUT_AUTO_Pause
            // 
            this.BUT_AUTO_Pause.Enabled = false;
            this.BUT_AUTO_Pause.Location = new System.Drawing.Point(540, 189);
            this.BUT_AUTO_Pause.Name = "BUT_AUTO_Pause";
            this.BUT_AUTO_Pause.Size = new System.Drawing.Size(104, 37);
            this.BUT_AUTO_Pause.TabIndex = 76;
            this.BUT_AUTO_Pause.Text = "Pause";
            this.BUT_AUTO_Pause.UseVisualStyleBackColor = true;
            this.BUT_AUTO_Pause.Click += new System.EventHandler(this.BUT_AUTO_Pause_Click);
            // 
            // BUT_AUTO_Stop
            // 
            this.BUT_AUTO_Stop.Enabled = false;
            this.BUT_AUTO_Stop.Location = new System.Drawing.Point(540, 141);
            this.BUT_AUTO_Stop.Name = "BUT_AUTO_Stop";
            this.BUT_AUTO_Stop.Size = new System.Drawing.Size(104, 37);
            this.BUT_AUTO_Stop.TabIndex = 76;
            this.BUT_AUTO_Stop.Text = "Stop";
            this.BUT_AUTO_Stop.UseVisualStyleBackColor = true;
            this.BUT_AUTO_Stop.Click += new System.EventHandler(this.BUT_AUTO_Stop_Click);
            // 
            // BUT_AUTO_Start
            // 
            this.BUT_AUTO_Start.Location = new System.Drawing.Point(540, 92);
            this.BUT_AUTO_Start.Name = "BUT_AUTO_Start";
            this.BUT_AUTO_Start.Size = new System.Drawing.Size(104, 37);
            this.BUT_AUTO_Start.TabIndex = 11;
            this.BUT_AUTO_Start.Text = "Start";
            this.BUT_AUTO_Start.UseVisualStyleBackColor = true;
            this.BUT_AUTO_Start.Click += new System.EventHandler(this.BUT_AUTO_Start_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label29.Location = new System.Drawing.Point(357, 83);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(85, 13);
            this.label29.TabIndex = 75;
            this.label29.Text = "Wiederholungen";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label28.Location = new System.Drawing.Point(463, 278);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(12, 13);
            this.label28.TabIndex = 74;
            this.label28.Text = "x";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label27.Location = new System.Drawing.Point(393, 368);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(12, 13);
            this.label27.TabIndex = 72;
            this.label27.Text = "x";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label3.Location = new System.Drawing.Point(141, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Accel";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label4.Location = new System.Drawing.Point(269, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Decel";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label22.Enabled = false;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(154, 551);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(74, 25);
            this.label22.TabIndex = 64;
            this.label22.Text = "ENDE";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label21.Enabled = false;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(149, 42);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(86, 25);
            this.label21.TabIndex = 63;
            this.label21.Text = "START";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label25.Location = new System.Drawing.Point(393, 165);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(12, 13);
            this.label25.TabIndex = 61;
            this.label25.Text = "x";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label8.Location = new System.Drawing.Point(112, 141);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 31;
            this.label8.Text = "Delay [ms]";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label13.Location = new System.Drawing.Point(112, 342);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 13);
            this.label13.TabIndex = 58;
            this.label13.Text = "Delay [ms]";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label9.Location = new System.Drawing.Point(112, 241);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 40;
            this.label9.Text = "Delay [ms]";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label17.Location = new System.Drawing.Point(112, 501);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 13);
            this.label17.TabIndex = 49;
            this.label17.Text = "Delay [ms]";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(20, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(503, 562);
            this.pictureBox1.TabIndex = 62;
            this.pictureBox1.TabStop = false;
            // 
            // LABEL_AUTO_Fehler
            // 
            this.LABEL_AUTO_Fehler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.LABEL_AUTO_Fehler.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LABEL_AUTO_Fehler.ForeColor = System.Drawing.SystemColors.GrayText;
            this.LABEL_AUTO_Fehler.Location = new System.Drawing.Point(11, 127);
            this.LABEL_AUTO_Fehler.Name = "LABEL_AUTO_Fehler";
            this.LABEL_AUTO_Fehler.Size = new System.Drawing.Size(182, 20);
            this.LABEL_AUTO_Fehler.TabIndex = 94;
            this.LABEL_AUTO_Fehler.Text = "Fehler";
            this.LABEL_AUTO_Fehler.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LABEL_AUTO_Warnung
            // 
            this.LABEL_AUTO_Warnung.BackColor = System.Drawing.Color.LightYellow;
            this.LABEL_AUTO_Warnung.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LABEL_AUTO_Warnung.ForeColor = System.Drawing.SystemColors.GrayText;
            this.LABEL_AUTO_Warnung.Location = new System.Drawing.Point(11, 99);
            this.LABEL_AUTO_Warnung.Name = "LABEL_AUTO_Warnung";
            this.LABEL_AUTO_Warnung.Size = new System.Drawing.Size(182, 20);
            this.LABEL_AUTO_Warnung.TabIndex = 94;
            this.LABEL_AUTO_Warnung.Text = "Warnung";
            this.LABEL_AUTO_Warnung.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox6);
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Controls.Add(this.groupBox7);
            this.groupBox3.Location = new System.Drawing.Point(12, 132);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(219, 491);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Allgemein";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.LABEL_AUTO_Fehler);
            this.groupBox6.Controls.Add(this.BUT_MANUELL_RESET);
            this.groupBox6.Controls.Add(this.LABEL_AUTO_Warnung);
            this.groupBox6.Controls.Add(this.BUT_MANUELL_Nullen);
            this.groupBox6.Controls.Add(this.COM_TB_STATUS);
            this.groupBox6.Location = new System.Drawing.Point(7, 322);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(204, 157);
            this.groupBox6.TabIndex = 92;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Status";
            // 
            // BUT_MANUELL_RESET
            // 
            this.BUT_MANUELL_RESET.Location = new System.Drawing.Point(105, 49);
            this.BUT_MANUELL_RESET.Name = "BUT_MANUELL_RESET";
            this.BUT_MANUELL_RESET.Size = new System.Drawing.Size(88, 38);
            this.BUT_MANUELL_RESET.TabIndex = 98;
            this.BUT_MANUELL_RESET.Text = "Fehler zurücksetzen";
            this.BUT_MANUELL_RESET.UseVisualStyleBackColor = true;
            this.BUT_MANUELL_RESET.Click += new System.EventHandler(this.BUT_MANUELL_RESET_Click);
            // 
            // COM_TB_STATUS
            // 
            this.COM_TB_STATUS.BackColor = System.Drawing.SystemColors.Control;
            this.COM_TB_STATUS.Enabled = false;
            this.COM_TB_STATUS.ForeColor = System.Drawing.SystemColors.GrayText;
            this.COM_TB_STATUS.Location = new System.Drawing.Point(11, 19);
            this.COM_TB_STATUS.Name = "COM_TB_STATUS";
            this.COM_TB_STATUS.Size = new System.Drawing.Size(184, 20);
            this.COM_TB_STATUS.TabIndex = 97;
            this.COM_TB_STATUS.TabStop = false;
            this.COM_TB_STATUS.Text = "???";
            this.COM_TB_STATUS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label51);
            this.groupBox5.Controls.Add(this.label50);
            this.groupBox5.Controls.Add(this.label49);
            this.groupBox5.Controls.Add(this.label48);
            this.groupBox5.Controls.Add(this.label47);
            this.groupBox5.Controls.Add(this.label46);
            this.groupBox5.Controls.Add(this.label45);
            this.groupBox5.Controls.Add(this.label44);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.RADIO_mg);
            this.groupBox5.Controls.Add(this.label41);
            this.groupBox5.Controls.Add(this.label38);
            this.groupBox5.Controls.Add(this.label43);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.RADIO_mm);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.RADIO_Steps);
            this.groupBox5.Controls.Add(this.label24);
            this.groupBox5.Controls.Add(this.label40);
            this.groupBox5.Controls.Add(this.RADIO_Rad);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.label42);
            this.groupBox5.Controls.Add(this.label39);
            this.groupBox5.Location = new System.Drawing.Point(7, 22);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(204, 165);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Einheiten";
            // 
            // label51
            // 
            this.label51.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label51.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label51.Location = new System.Drawing.Point(6, 126);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(191, 2);
            this.label51.TabIndex = 106;
            // 
            // label50
            // 
            this.label50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label50.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label50.Location = new System.Drawing.Point(6, 100);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(191, 2);
            this.label50.TabIndex = 105;
            // 
            // label49
            // 
            this.label49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label49.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label49.Location = new System.Drawing.Point(6, 74);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(191, 2);
            this.label49.TabIndex = 104;
            // 
            // label48
            // 
            this.label48.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label48.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label48.Location = new System.Drawing.Point(29, 26);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(2, 122);
            this.label48.TabIndex = 103;
            // 
            // label47
            // 
            this.label47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label47.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label47.Location = new System.Drawing.Point(81, 26);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(2, 122);
            this.label47.TabIndex = 102;
            // 
            // label46
            // 
            this.label46.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label46.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label46.Location = new System.Drawing.Point(140, 26);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(2, 122);
            this.label46.TabIndex = 101;
            // 
            // label45
            // 
            this.label45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label45.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label45.Location = new System.Drawing.Point(6, 48);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(191, 2);
            this.label45.TabIndex = 98;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.SystemColors.Control;
            this.label44.Location = new System.Drawing.Point(161, 133);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(21, 13);
            this.label44.TabIndex = 100;
            this.label44.Text = "mg";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.SystemColors.Control;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(147, 28);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 13);
            this.label16.TabIndex = 100;
            this.label16.Text = "Beschl.";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RADIO_mg
            // 
            this.RADIO_mg.AutoSize = true;
            this.RADIO_mg.Location = new System.Drawing.Point(11, 133);
            this.RADIO_mg.Name = "RADIO_mg";
            this.RADIO_mg.Size = new System.Drawing.Size(14, 13);
            this.RADIO_mg.TabIndex = 6;
            this.RADIO_mg.UseVisualStyleBackColor = true;
            this.RADIO_mg.CheckedChanged += new System.EventHandler(this.RADIO_AD_CheckedChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.SystemColors.Control;
            this.label41.Location = new System.Drawing.Point(153, 107);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(36, 13);
            this.label41.TabIndex = 100;
            this.label41.Text = "mm/s²";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.SystemColors.Control;
            this.label38.Location = new System.Drawing.Point(148, 81);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(47, 13);
            this.label38.TabIndex = 100;
            this.label38.Text = "Steps/s²";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.SystemColors.Control;
            this.label43.Location = new System.Drawing.Point(95, 133);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(33, 13);
            this.label43.TabIndex = 99;
            this.label43.Text = "mm/s";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.SystemColors.Control;
            this.label20.Location = new System.Drawing.Point(141, 57);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(61, 13);
            this.label20.TabIndex = 100;
            this.label20.Text = "0.01Rad/s²";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RADIO_mm
            // 
            this.RADIO_mm.AutoSize = true;
            this.RADIO_mm.Location = new System.Drawing.Point(11, 107);
            this.RADIO_mm.Name = "RADIO_mm";
            this.RADIO_mm.Size = new System.Drawing.Size(14, 13);
            this.RADIO_mm.TabIndex = 4;
            this.RADIO_mm.UseVisualStyleBackColor = true;
            this.RADIO_mm.CheckedChanged += new System.EventHandler(this.RADIO_AD_CheckedChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.SystemColors.Control;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(90, 28);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 13);
            this.label14.TabIndex = 99;
            this.label14.Text = "Speed";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.SystemColors.Control;
            this.label19.Location = new System.Drawing.Point(82, 55);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(58, 13);
            this.label19.TabIndex = 99;
            this.label19.Text = "0.01Rad/s";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RADIO_Steps
            // 
            this.RADIO_Steps.AutoSize = true;
            this.RADIO_Steps.Checked = true;
            this.RADIO_Steps.Location = new System.Drawing.Point(11, 81);
            this.RADIO_Steps.Name = "RADIO_Steps";
            this.RADIO_Steps.Size = new System.Drawing.Size(14, 13);
            this.RADIO_Steps.TabIndex = 4;
            this.RADIO_Steps.TabStop = true;
            this.RADIO_Steps.UseVisualStyleBackColor = true;
            this.RADIO_Steps.CheckedChanged += new System.EventHandler(this.RADIO_AD_CheckedChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.SystemColors.Control;
            this.label24.Location = new System.Drawing.Point(89, 81);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(44, 13);
            this.label24.TabIndex = 99;
            this.label24.Text = "Steps/s";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.SystemColors.Control;
            this.label40.Location = new System.Drawing.Point(95, 107);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(33, 13);
            this.label40.TabIndex = 99;
            this.label40.Text = "mm/s";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RADIO_Rad
            // 
            this.RADIO_Rad.AutoSize = true;
            this.RADIO_Rad.Location = new System.Drawing.Point(11, 55);
            this.RADIO_Rad.Name = "RADIO_Rad";
            this.RADIO_Rad.Size = new System.Drawing.Size(14, 13);
            this.RADIO_Rad.TabIndex = 5;
            this.RADIO_Rad.UseVisualStyleBackColor = true;
            this.RADIO_Rad.CheckedChanged += new System.EventHandler(this.RADIO_AD_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.SystemColors.Control;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(30, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 31;
            this.label12.Text = "Position";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.SystemColors.Control;
            this.label18.Location = new System.Drawing.Point(39, 55);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(34, 13);
            this.label18.TabIndex = 31;
            this.label18.Text = "Steps";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.SystemColors.Control;
            this.label23.Location = new System.Drawing.Point(39, 81);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(34, 13);
            this.label23.TabIndex = 31;
            this.label23.Text = "Steps";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.SystemColors.Control;
            this.label42.Location = new System.Drawing.Point(45, 133);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(23, 13);
            this.label42.TabIndex = 31;
            this.label42.Text = "mm";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.SystemColors.Control;
            this.label39.Location = new System.Drawing.Point(45, 107);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(23, 13);
            this.label39.TabIndex = 31;
            this.label39.Text = "mm";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label35);
            this.groupBox7.Controls.Add(this.label34);
            this.groupBox7.Controls.Add(this.TB_Mech_VW);
            this.groupBox7.Controls.Add(this.TB_Mech_Motor);
            this.groupBox7.Controls.Add(this.TB_Mech_SL);
            this.groupBox7.Controls.Add(this.label36);
            this.groupBox7.Controls.Add(this.label33);
            this.groupBox7.Controls.Add(this.label32);
            this.groupBox7.Controls.Add(this.label31);
            this.groupBox7.Location = new System.Drawing.Point(7, 199);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(204, 112);
            this.groupBox7.TabIndex = 12;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Mechnik";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.SystemColors.Control;
            this.label35.Location = new System.Drawing.Point(133, 53);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(67, 13);
            this.label35.TabIndex = 90;
            this.label35.Text = "Steps/Umdr.";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.SystemColors.Control;
            this.label34.Location = new System.Drawing.Point(133, 76);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(56, 13);
            this.label34.TabIndex = 91;
            this.label34.Text = "mm/Umdr.";
            // 
            // TB_Mech_VW
            // 
            this.TB_Mech_VW.Enabled = false;
            this.TB_Mech_VW.Location = new System.Drawing.Point(82, 73);
            this.TB_Mech_VW.Name = "TB_Mech_VW";
            this.TB_Mech_VW.Size = new System.Drawing.Size(48, 20);
            this.TB_Mech_VW.TabIndex = 88;
            this.TB_Mech_VW.Text = "54";
            this.TB_Mech_VW.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TB_Mech_Motor
            // 
            this.TB_Mech_Motor.Enabled = false;
            this.TB_Mech_Motor.Location = new System.Drawing.Point(82, 49);
            this.TB_Mech_Motor.Name = "TB_Mech_Motor";
            this.TB_Mech_Motor.Size = new System.Drawing.Size(48, 20);
            this.TB_Mech_Motor.TabIndex = 87;
            this.TB_Mech_Motor.Text = "200";
            this.TB_Mech_Motor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TB_Mech_SL
            // 
            this.TB_Mech_SL.Enabled = false;
            this.TB_Mech_SL.Location = new System.Drawing.Point(82, 25);
            this.TB_Mech_SL.Name = "TB_Mech_SL";
            this.TB_Mech_SL.Size = new System.Drawing.Size(48, 20);
            this.TB_Mech_SL.TabIndex = 86;
            this.TB_Mech_SL.Text = "460";
            this.TB_Mech_SL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.SystemColors.Control;
            this.label36.Location = new System.Drawing.Point(133, 28);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(23, 13);
            this.label36.TabIndex = 89;
            this.label36.Text = "mm";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.SystemColors.Control;
            this.label33.Location = new System.Drawing.Point(18, 77);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(61, 13);
            this.label33.TabIndex = 85;
            this.label33.Text = "Verfahrweg";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.SystemColors.Control;
            this.label32.Location = new System.Drawing.Point(45, 53);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(34, 13);
            this.label32.TabIndex = 85;
            this.label32.Text = "Motor";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.SystemColors.Control;
            this.label31.Location = new System.Drawing.Point(5, 28);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(74, 13);
            this.label31.TabIndex = 83;
            this.label31.Text = "Schlittenlänge";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.LABEL_COM_Status);
            this.groupBox4.Controls.Add(this.COMBO_COM_Schnittstelle);
            this.groupBox4.Controls.Add(this.BUT_COM_Trennen);
            this.groupBox4.Controls.Add(this.BUT_COM_Verbinden);
            this.groupBox4.Location = new System.Drawing.Point(12, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(219, 114);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Verbindung";
            // 
            // LABEL_COM_Status
            // 
            this.LABEL_COM_Status.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.LABEL_COM_Status.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LABEL_COM_Status.ForeColor = System.Drawing.SystemColors.ControlText;
            this.LABEL_COM_Status.Location = new System.Drawing.Point(13, 75);
            this.LABEL_COM_Status.Name = "LABEL_COM_Status";
            this.LABEL_COM_Status.Size = new System.Drawing.Size(93, 21);
            this.LABEL_COM_Status.TabIndex = 95;
            this.LABEL_COM_Status.Text = "Gertrennt";
            this.LABEL_COM_Status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // COMBO_COM_Schnittstelle
            // 
            this.COMBO_COM_Schnittstelle.FormattingEnabled = true;
            this.COMBO_COM_Schnittstelle.Location = new System.Drawing.Point(13, 40);
            this.COMBO_COM_Schnittstelle.Name = "COMBO_COM_Schnittstelle";
            this.COMBO_COM_Schnittstelle.Size = new System.Drawing.Size(93, 21);
            this.COMBO_COM_Schnittstelle.TabIndex = 2;
            this.COMBO_COM_Schnittstelle.Click += new System.EventHandler(this.Update_Schnittstellen);
            // 
            // BUT_COM_Trennen
            // 
            this.BUT_COM_Trennen.Enabled = false;
            this.BUT_COM_Trennen.Location = new System.Drawing.Point(115, 75);
            this.BUT_COM_Trennen.Name = "BUT_COM_Trennen";
            this.BUT_COM_Trennen.Size = new System.Drawing.Size(93, 21);
            this.BUT_COM_Trennen.TabIndex = 1;
            this.BUT_COM_Trennen.Text = "Trennen";
            this.BUT_COM_Trennen.UseVisualStyleBackColor = true;
            this.BUT_COM_Trennen.Click += new System.EventHandler(this.BUT_COM_Trennen_Click);
            // 
            // BUT_COM_Verbinden
            // 
            this.BUT_COM_Verbinden.Location = new System.Drawing.Point(115, 39);
            this.BUT_COM_Verbinden.Name = "BUT_COM_Verbinden";
            this.BUT_COM_Verbinden.Size = new System.Drawing.Size(93, 21);
            this.BUT_COM_Verbinden.TabIndex = 0;
            this.BUT_COM_Verbinden.Text = "Verbinden";
            this.BUT_COM_Verbinden.UseVisualStyleBackColor = true;
            this.BUT_COM_Verbinden.Click += new System.EventHandler(this.BUT_COM_Verbinden_Click);
            // 
            // Timer1
            // 
            this.Timer1.Interval = 50;
            this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1061, 637);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Linearantrieb PC-Prog";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_MANUELL_AccDec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_MANUELL_Steps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_MANUELL_Speed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MANUELL_SLIDER_Steps2)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_REPEAT4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_REPEAT2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_REPEAT5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_DELAY4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_DELAY3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_DELAY2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_DECEL4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_SPEED4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_ACCEL4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_DECEL3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_SPEED3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_ACCEL3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_DECEL2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_SPEED2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_ACCEL2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_STEP4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_STEP3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_STEP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_DECEL1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_SPEED1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_DELAY1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_ACCEL1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_STEP1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AUTO_NUM_DELAY5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.IO.Ports.SerialPort SerPort1;
        private System.Windows.Forms.Button BUT_MANUELL_Nullen;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button BUT_AUTO_Stop;
        private System.Windows.Forms.Button BUT_AUTO_Start;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox AUTO_TB_REPEAT4;
        private System.Windows.Forms.TextBox AUTO_TB_REPEAT2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button BUT_AUTO_Pause;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox COMBO_COM_Schnittstelle;
        private System.Windows.Forms.Button BUT_COM_Trennen;
        private System.Windows.Forms.Button BUT_COM_Verbinden;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox TB_Mech_VW;
        private System.Windows.Forms.TextBox TB_Mech_Motor;
        private System.Windows.Forms.TextBox TB_Mech_SL;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_DELAY5;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_STEP1;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_ACCEL1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_REPEAT4;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_REPEAT2;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_REPEAT5;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_DELAY4;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_DELAY3;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_DELAY2;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_DECEL4;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_SPEED4;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_ACCEL4;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_DECEL3;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_SPEED3;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_ACCEL3;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_DECEL2;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_SPEED2;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_ACCEL2;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_STEP4;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_STEP3;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_STEP2;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_DECEL1;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_SPEED1;
        private System.Windows.Forms.NumericUpDown AUTO_NUM_DELAY1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label LABEL_AUTO_Fehler;
        private System.Windows.Forms.Label LABEL_AUTO_Warnung;
        private System.Windows.Forms.Timer Timer1;
        private System.Windows.Forms.Label LABEL_COM_Status;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown NUM_MANUELL_Steps;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown NUM_MANUELL_Speed;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.RadioButton RADIO_mg;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.RadioButton RADIO_mm;
        private System.Windows.Forms.RadioButton RADIO_Steps;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.RadioButton RADIO_Rad;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox AUTO_TB_REPEAT5;
        private System.Windows.Forms.TrackBar MANUELL_SLIDER_Steps2;
        private System.Windows.Forms.TextBox COM_TB_STATUS;
        private System.Windows.Forms.TextBox MANUELL_TB_ISTPOS;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button BUT_MANUELL_RESET;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.NumericUpDown NUM_MANUELL_AccDec;
    }
}

